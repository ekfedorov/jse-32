package ru.ekfedorov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.dto.Domain;
import ru.ekfedorov.tm.enumerated.Role;

import java.io.FileOutputStream;

public class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Save JSON(fasterxml) data.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "data-json-fasterxml-save";
    }

    @NotNull
    public Role[] commandRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON SAVE]");
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
        @NotNull final String json = objectWriter.writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_JSON);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}
